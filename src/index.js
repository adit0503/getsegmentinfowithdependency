// global imports
const path = require("path");

// project imports
const Config = require("./helper/Config");
const Util = require("./helper/Util");
const C3ApiCaller = require("./helper/C3ApiCaller");
const LogHelper = require("./helper/LogHelper");

// set configurations
const rootDirPath = path.dirname(__dirname);
const configFilePath = path.join(rootDirPath, "config.json");
const config = Config.Config.getConfig(configFilePath);
const namespace = config.keysettings.namespace;

// HTTP request handler
const c3ApiCaller = new C3ApiCaller.C3ApiCaller(
    config.keysettings.c3servername,
    config.keysettings.logincreds,
    namespace
);

// helps log info/error to logs
const logger = LogHelper.getLogger();

/*
SEGMENTS Obj:
{
    segmentID: {
        Name:
        Create Date:
        Update Date:
        TI: [{
            TI Name:
            TI Id:
            TI created date:
            TI state:
        }],
        SI: [{
            SI Name:
            SI ID:
            SI Date:
            SI Status:
        }],
    },
}
*/

/*********************** Get Required Segment Details *************************** */
let SEGMENTS = {};

function getSegment(editUrl) {
    /**
     * A function to get segment ids for each TI & SI
     * @param {string} editUrl - The TI/SI edit url
     * @returns {array} -  A list of segmentId strings
     */

    let editOptions = {
        uri: editUrl,
        method: "GET",
    };

    return new Promise((resolve, reject) => {
        let segment;
        setTimeout(() => {
            c3ApiCaller
                .getApiData(editOptions)
                .then((result) => {
                    if (typeof result === "string") {
                        result = JSON.parse(result);
                        if ("payload" in result) {
                            let segments = "";
                            result.payload.states.forEach((state) => {
                                segments = state.ref;
                            });
                            segment = segments.split(",");
                        }
                    }
                    resolve(segment);
                })
                .catch((err) => {
                    logger.error("Error in getSegment() " + err);
                    reject(0);
                });
        }, 300);
    });
}

function getAllSegments() {
    /**
     * Get all Segments from the API endpoint
     * @param
     * @returns {promise} - Promise object represents a Array of all Segments
     */
    let url =
        config.keysettings.c3servername + config.keysettings.c3api.segments.all;
    let options = {
        uri: Util.Util.simpleTemplateReplace(url, { namespace: namespace }),
        method: "GET",
    };
    return new Promise((resolve, reject) => {
        c3ApiCaller
            .getApiData(options)
            .then((result) => {
                logger.info("Data recieved from API of All Segments");
                let allSegments = [];
                if (typeof result === "string") {
                    allSegments = JSON.parse(result);
                } else {
                    allSegments = result;
                }
                resolve(allSegments);
            })
            .catch((err) => {
                logger.error("Error in getAllSegments() " + err);
                reject(0);
            });
    });
}

async function getReqSegments() {
    /**
     * Get all Segments with desired object properties
     * @param
     * @returns
     */
    let data;
    try {
        data = await getAllSegments();
        for (let segment of data) {
            SEGMENTS[segment.id] = {
                name: segment.name,
                creationTime: Util.Util.formattedDate(segment.creationTime),
                lastUpdatedTime: Util.Util.formattedDate(segment.lastUpdatedTime),
                TI: [],
                SI: [],
            };
        }
    } catch (err) {
        logger.error("Error in getRegSegments() " + err);
    }
}


/*********************** Get All TIs for each Segment *************************** */
function getAllTIs() {
    /**
     * Get all TI
     * @param
     * @returns {Promise} Promise object represents an array of all TIs
     */

    let url = config.keysettings.c3servername + config.keysettings.c3api.ti.all;
    let options = {
        "uri": Util.Util.simpleTemplateReplace(url, { "namespace": namespace }),
        "method": "GET"
    };

    return new Promise((resolve, reject) => {
        c3ApiCaller
            .getApiData(options)
            .then((result) => {
                logger.info("Data recieved from API of all TIs");
                let allTIs = [];
                if (typeof (result) === "string") {
                    allTIs = JSON.parse(result);
                }
                else {
                    allTIs = result;
                }
                resolve(allTIs);
            }).catch((err) => {
                logger.error("Error in getAllTIs() " + err);
                reject(0);
            });
    });
}

async function getAllTISegments() {
    /**
     * Populate Global SEGMENTS object with TI
     * @param
     * @returns
     */
    let editUrl = config.keysettings.c3servername + config.keysettings.c3api.ti.edit;

    let data
    try {
        data = await getAllTIs()

        let i = 0;
        for (let ti of data) {
            console.log(`processing TI ${i} :  ${ti.defitem.name}`)

            let editUrlLocal = Util.Util.simpleTemplateReplace(editUrl, { "namespace": namespace, "journeyId": ti.defitem.id });
            let segments
            try {
                segments = await getSegment(editUrlLocal)

                for (let segment of segments) {
                    SEGMENTS[segment].TI.push({
                        "name": ti.defitem.name,
                        "id": ti.defitem.id,
                        "creationTime": Util.Util.formattedDate(ti.defitem.creationTime),
                        "state": ti.defitem.state
                    })
                }
            }
            catch (err) {
                logger.error("Error in getSegment() inside getAllTISegments() " + err);
            }
            i += 1
        }
    }
    catch (err) {
        logger.error("Error in getAllTIs() " + err);
    }
}


/*********************** Get All SIs for each Segment *************************** */
function getAllSIs() {
    let url = config.keysettings.c3servername + config.keysettings.c3api.si.all;
    let options = {
        "uri": Util.Util.simpleTemplateReplace(url, { "namespace": namespace }),
        "method": "GET"
    }

    return new Promise((resolve, reject) => {
        c3ApiCaller.getApiData(options)
        .then((result) => {
            logger.info("Data recieved for All SI");
            let allSIs = [];
            if (typeof (result) === "string") {
                allSIs = JSON.parse(result);
            }
            else {
                allSIs = result;
            }
            resolve(allSIs);
        }).catch((err) => {
            logger.error("Error in getAllSIs" + err);
            reject(0);
        });
    });
}

async function getAllSISegments() {
    let editUrl = config.keysettings.c3servername + config.keysettings.c3api.si.edit;
    
    let data
    try{
        data = await getAllSIs()
        for(let si of data){
            let editUrlLocal = Util.Util.simpleTemplateReplace(editUrl, { "namespace": namespace, "journeyId": si.defitem.id });
            let segments
            try {
                segments = await getSegment(editUrlLocal)
                
                for (let segment of segments) {
                    SEGMENTS[segment].SI.push({
                        "name": si.defitem.name,
                        "id": si.defitem.id,
                        "creationTime": Util.Util.formattedDate(si.defitem.creationTime),
                        "status": si.defitem.status
                    })
                }
            }
            catch(err){
                logger.error("Error in getSegment() " + err);
            }
        }
    }
    catch(err) {
        logger.error("Error in getAllSIs" + err);
    }
}


/*********************** Debugging and Testing ********************************** */
function checkSegments() {
    segments_notFound = ["segment+7dd3a477-4dd4-4f88-acb3-3e5767b496d5", "segment+1812a2d9-e9ab-4be6-99b5-4df0d15985ea", "segment+25aed510-3975-4b79-b2f5-e802e54465b7", "segment+0ef45944-957a-4207-aa78-c6dcb3420440"]
    for (let segment of segments_notFound) {
        if (!(segment in SEGMENTS)) {
            console.log(`${segment} not found`)
        }
    }
}


/*********************** Process Final Result ********************************** */
async function finalResult() {
    /**
     * MAIN function to control all other functions in the module
     * @param
     * @returns 
     */

    try {
        await getReqSegments()
        logger.info("All Segments Collected")

        // checkSegments()

        // await getAllTISegments()
        // logger.info("All TIs processed")

        await getAllSISegments()

    }
    catch (err) {
        logger.error("Error in finalResult() " + err);
    }
}

finalResult()