const request = require("request")

const model = require("../model/Model")


function isEmpty(obj) {
    for (var prop in obj) {
        if (obj.hasOwnProperty(prop)) {
            return false;
        }
    }
    return true;
}

class C3ApiCaller {
    constructor(c3url="", credential, namespace="") {
        this._c3url = c3url
        this._credential = credential
        this._namespace = namespace

        this._sessionTimeOut = 5
    }
    
    _sessionCookies = []

    _getAuthenticationSessionId() {
        let options = {
            uri: this._c3url + "/login?namespace=" + this._namespace,
            method: 'POST',
            headers: {
                "Content-Type": "text/plain"
            },
            body: this._credential
        };

        return new Promise((resolve, reject) => {

            request(options, function (error, response, body) {
                
                if (error) {
                    reject(error);
                }
                if (!response) {
                    reject(error);
                }
                if (response.statusCode !== 200) {
                    reject('Invalid status code <' + response.statusCode + '>');
                }

                if (!error && response.statusCode === 200) {
                    if (typeof body !== 'string' && "status" in body && body["status"] === "fail") {
                        reject(JSON.stringify(body));
                    }

                    var setcookie = response.headers["set-cookie"];
                    if (setcookie) {
                        var cookieArray = [];
                        for (let i = 0; i < setcookie.length; i++) {
                            cookieArray.push(setcookie[i].split(";")[0]);
                        }
                        resolve(cookieArray);
                    }
                }
            });
        });
    }

    _getSessionCookies() {
        
        return new Promise((resolve, reject) => {
            
            if (C3ApiCaller._sessionStartTime) {
                let currentDateTime = new Date();
                let diffMs = (currentDateTime.getTime() - C3ApiCaller._sessionStartTime.getTime());
                let diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000);
                
                if (diffMins > this._sessionTimeOut) {
                    C3ApiCaller._sessionCookies = [];
                    C3ApiCaller._sessionStartTime = new Date();
                }
            }

            if (!isEmpty(C3ApiCaller._sessionCookies)) {
                resolve(C3ApiCaller._sessionCookies);
            }
            else {
                this._getAuthenticationSessionId()
                .then((result) => {
                    C3ApiCaller._sessionCookies = result;
                    C3ApiCaller._sessionStartTime = new Date();
                    resolve(C3ApiCaller._sessionCookies);
                })
                .catch((err) => {
                    reject(err);
                });
            }
        });
    }

    getApiData(postDataOptions) {

        return new Promise((resolve, reject) => {

            this._getSessionCookies()
            .then((cookies) => {
                postDataOptions["headers"] = {
                    'Cookie': cookies.join("; "),
                    'Accept': '/',
                    'Connection': 'keep-alive'
                };

                request(postDataOptions, function (error, response, body) {
                    if (error) {
                        reject(model.status.fail);
                    }
                    if (response.statusCode !== 200) {
                        reject(model.status.fail);
                    }
                    if (!error && response.statusCode === 200) {
                        if (!body || body === "" || typeof body === 'undefined') {
                            body = model.status.success;
                        }
                        resolve(body);
                    }
                });
            })
            .catch((err) => {
                reject(model.status.fail);
            });
        });
    }
}

exports.C3ApiCaller = C3ApiCaller;
