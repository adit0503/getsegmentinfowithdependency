const path = require("path");
const fs = require("fs");
const { isNull } = require("util");


class Util {
    
    static simpleTemplateReplace(template, data) {
        const pattern = /\{{(.*?)\}}/g;       
        return template.replace(pattern, (match, token) => data[token]);
    }

    static formattedDate(timestamp) {
        if(timestamp){
            let date = new Date(timestamp);
            return (
                ("0" + date.getDay()).slice(-2) +
                "-" +
                ("0" + (date.getMonth() + 1)).slice(-2) +
                "-" +
                date.getFullYear()
            );
        }
        return ""
    }

    static writeObjectToFile(object) {
        fs.writeFileSync("./test/segments.json", JSON.stringify(object))
    }
}

Util.rootDirPath = path.dirname(path.dirname(__dirname));
exports.Util = Util