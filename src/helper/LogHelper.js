const path = require("path");
const util = require("util");

const winston = require("winston");


const transports = winston.transports;

function getLogger() {

    let constFormat = winston.format.combine(winston.format.timestamp(), winston.format.printf((info) => {
        const timestamp = info.timestamp.trim();
        const level = info.level;
        const message = (info.message || '').trim();
        const args = info[Symbol.for('splat')];
        const strArgs = (args || []).map((arg) => {
            return util.inspect(arg, {
                colors: false
            });
        }).join(' ');
        return `[${timestamp}] ${level} ${message} ${strArgs}`;
    }));

    const logger = winston.createLogger({
        level: 'info',
        defaultMeta: { service: 'Daily SI Reporting' },
        transports: [
            new transports.File({
                filename: path.join(path.dirname(path.dirname(__dirname)), 'logs', 'app.log'),
                maxsize: 5000000,
                maxFiles: 4,
                format: constFormat
            }),
            new transports.Console({ format: constFormat })
        ]
    });
    return logger;
}
exports.getLogger = getLogger;