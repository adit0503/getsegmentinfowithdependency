const fs = require("fs")


class Config {
    static getConfig(filePath) {
        if(filePath !== "" && fs.existsSync(filePath)) {
            return JSON.parse(fs.readFileSync(filePath, 'utf-8'))
        }
        return
    }
}

exports.Config = Config;