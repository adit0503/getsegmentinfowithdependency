class Auth {
    constructor(url = "", userid = "", password = "") {
        this.url = url
        this.userid = userid
        this.password = password
    }
}
exports.Auth = Auth;

var status;
(function (status) {
    status[status["fail"] = 0] = "fail"
    status[status["success"] = 1] = "success"
})(status = exports.status || (exports.status = {}));