// global imports
const path = require("path");
const fs = require("fs")

// project imports
const Config = require("./helper/Config");
const Util = require("./helper/Util");
const C3ApiCaller = require("./helper/C3ApiCaller");
const LogHelper = require("./helper/LogHelper");



/**
 * 1. Get all segments 
 * 2. add TI/SI to each segment
 * 3. save data to CSV
 */

// set configurations
const rootDirPath = path.dirname(__dirname);
const configFilePath = path.join(rootDirPath, "config.json");
const config = Config.Config.getConfig(configFilePath);
const namespace = config.keysettings.namespace;

// HTTP request handler
const c3ApiCaller = new C3ApiCaller.C3ApiCaller(
    config.keysettings.c3servername,
    config.keysettings.logincreds,
    namespace
);

// helps log info/error to logs
const logger = LogHelper.getLogger();

// global variables
const allURL = {
    "TI": config.keysettings.c3api.ti.all,
    "SI": config.keysettings.c3api.si.all
}
const editURL = {
    "TI": config.keysettings.c3api.ti.edit,
    "SI": config.keysettings.c3api.si.edit
} 
let INTERACTIONS = ["TI", "SI"]
let SEGMENTS = {};
let OUTPUT_FILE = path.dirname(__dirname)+"/src/analytics/Segments.csv"

/*
SEGMENTS Obj:
{
    segmentID: {
        Name:
        Create Date:
        Update Date:
        TI: [{
            TI Name:
            TI Id:
            TI created date:
            TI state:
        }],
        SI: [{
            SI Name:
            SI ID:
            SI Date:
            SI Status:
        }],
    },
}
*/

/**
 * A function to get segment ids for each TI & SIs
 * @param {string} editUrl - The TI/SI edit url
 * @returns {array} -  A list of segmentId strings
 */
function getSegment(editUrl) {
    let editOptions = {
        uri: editUrl,
        method: "GET",
    };

    return new Promise((resolve, reject) => {
        let segment;
        setTimeout(() => {
            c3ApiCaller
                .getApiData(editOptions)
                .then((result) => {
                    if (typeof result === "string") {
                        result = JSON.parse(result);
                        if ("payload" in result) {
                            let segments = "";
                            result.payload.states.forEach((state) => {
                                segments = state.ref;
                            });
                            segment = segments.split(",");
                        }
                    }
                    resolve(segment);
                })
                .catch((err) => {
                    logger.error("Error in getSegment() " + err);
                    reject(0);
                });
        }, 300);
    });
}

/**
 * Get all Segments from the API endpoint (1 call)
 * @returns {promise} - Promise object represents a Array of all Segments
 */
function getSegments() {
    let url = config.keysettings.c3servername + config.keysettings.c3api.segments.all;
    let options = {
        uri: Util.Util.simpleTemplateReplace(url, { namespace: namespace }),
        method: "GET",
    };
    return new Promise((resolve, reject) => {
        c3ApiCaller
            .getApiData(options)
            .then((result) => {
                logger.info("Data recieved from API for all Segments");
                let allSegments = [];
                if (typeof result === "string") {
                    allSegments = JSON.parse(result);
                } else {
                    allSegments = result;
                }
                resolve(allSegments);
            })
            .catch((err) => {
                logger.error("Error in getAllSegments() " + err);
                reject(0);
            });
    });
}

/**
 * Get all Segments with desired object properties
 */
async function getAllSegments() {
    let data;
    try {
        data = await getSegments();
        for (let segment of data) {
            SEGMENTS[segment.id] = {
                "name": segment.name,
                "creationTime": segment.creationTime,
                "lastUpdatedTime": segment.lastUpdatedTime,
                "TI": [],
                "SI": [],
            };
        }
    } catch (err) {
        logger.error("Error in getReqSegments() " + err);
    }
}

/**
 * Get all TI/SI (1 call)
 * @param {string} type - Type of Interaction TI/SI
 * @returns {Promise} Promise object represents an array of all TI/SI
 */
function getAllInteractions(type) {
    let url = config.keysettings.c3servername + allURL[type]
    let options = {
        "uri": Util.Util.simpleTemplateReplace(url, { "namespace": namespace }),
        "method": "GET"
    };

    return new Promise((resolve, reject) => {
        c3ApiCaller
            .getApiData(options)
            .then((result) => {
                logger.info(`Data recieved from API for all ${type}s`);
                let allInteractions = [];
                if (typeof (result) === "string") {
                    allInteractions = JSON.parse(result);
                }
                else {
                    allInteractions = result;
                }
                resolve(allInteractions);
            }).catch((err) => {
                logger.error("Error in getAllInteractions() " + err);
                reject(0);
            });
    });
}

/**
 * Update SEGMENT object with TI/SI details
 * @param {string} type - Type of Interaction TI/SI
 */
async function updateSegments(type) {
    let editUrl = config.keysettings.c3servername + editURL[type]

    let interactions
    try{
        interactions = await getAllInteractions(type)

        let i = 0;
        for (let interaction of interactions) {
            console.log(`processing ${type} ${i} :  ${interaction.defitem.name}`)

            let editUrlLocal = Util.Util.simpleTemplateReplace(editUrl, { "namespace": namespace, "journeyId": interaction.defitem.id });
            let segments
            try {
                segments = await getSegment(editUrlLocal)

                for (let segment of segments) {
                    SEGMENTS[segment][type].push({
                        "name": interaction.defitem.name,
                        "id": interaction.defitem.id,
                        "creationTime": interaction.defitem.creationTime,
                        "status": interaction.defitem.status || interaction.defitem.state
                    })
                }
            }
            catch (err) {
                logger.error("Error in getSegment() inside getAllTISegments() " + err);
            }
            i += 1
        }
    }
    catch (err) {
        logger.error("Error in getAllTIs() " + err);
    }
}

/**
 * Check if segments exisit in SEGMENTS obj
 */
function checkSegments() {
    segments_notFound = ["segment+7dd3a477-4dd4-4f88-acb3-3e5767b496d5", "segment+1812a2d9-e9ab-4be6-99b5-4df0d15985ea", "segment+25aed510-3975-4b79-b2f5-e802e54465b7", "segment+0ef45944-957a-4207-aa78-c6dcb3420440"]
    for (let segment of segments_notFound) {
        if (!(segment in SEGMENTS)) {
            console.log(`${segment} not found`)
        }
    }
}

/**
 * Write SEGMENTS object to CSV
 */
function writeToCSV() {    
    let csvData = "SegmentId,SegmentName,SegmentCreateDate,SegmentUpdateDate,Type,ID,Name,CreateDate,Status" + "\n";
    for (let [id, segment] of Object.entries(SEGMENTS)) {
        let row = [id, segment.name, segment.creationTime, segment.lastUpdatedTime].join(',')
        
        let flag = true
        segment.TI.forEach((elem) => {
            csvData += `${row}` + ',TI,' + [elem.id, elem.name, elem.creationTime, elem.status].join(',') + '\n';
            flag = false
        })
        segment.SI.forEach((elem) => {
            csvData += `${row}` + ',SI,' + [elem.id, elem.name, elem.creationTime, elem.status].join(',') + '\n';
            flag = false
        })

        if (flag) {
            csvData += row + ',,,,,\n';
        }
    }
    fs.writeFileSync(OUTPUT_FILE, csvData)
}

/**
 * MAIN function to control all other functions in this module
 */
async function finalResult() {
    try {

        logger.info("********* START ********")

        await getAllSegments()
        logger.info("All Segments Collected")

        for(let interaction of INTERACTIONS){
            await updateSegments(interaction)
            logger.info(`All ${interaction}s Processed`)
        }

        writeToCSV()
        logger.info("All Segments Recorded in CSV")

        logger.info("********* END *********")
    }
    catch (err) {
        logger.error("Error in finalResult() " + err);
    }
}
finalResult()